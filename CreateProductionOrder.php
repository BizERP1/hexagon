<?php
	
	$dataTable=1;
	
	include('includes/SQL_CommonFunctions.inc');
	include('includes/session.inc');
	$Title = _('Production Order');	
	include('includes/header.inc');
		
	//Check if there is an argument value in the url
	if(isset($_GET['OrderNumber']))
	{
		$_SESSION['SalesOrderForProduction'] = $_GET['OrderNumber']; //Assign sales order number to a session variable
		$_POST['SONumber'] = $_GET['OrderNumber']; //Assign sales order number to Sales Order Number field
		
		//Select delivery date of sales order
		$sql0 = "SELECT deliverydate FROM salesorders WHERE orderno = '".$_SESSION['SalesOrderForProduction']."'"; 
		$result0 = DB_query($sql0);		
		$myrow0 = DB_fetch_array($result0);		
		$_POST['SODeliveryDate'] = $myrow0[0];
		
		//Select list of items in the sales order
		$sql1 = "SELECT stkcode FROM salesorderdetails WHERE orderno = '".$_SESSION['SalesOrderForProduction']."'"; 
		$result1 = DB_query($sql1);
						
		//Select list of transport modes
		$sql8 = "SELECT * FROM transportationmodes"; 
		$result8 = DB_query($sql8);	
		$numrows8 = DB_num_rows($result8);		
		
	}
	
	//If Delete link is clicked
	if(isset($_GET['SelectedProductionOrderForDelete']))
	{
		$_SESSION['Status'] = 2;
		$_POST['SONumber'] = $_SESSION['SalesOrderForProduction'];		
		$_SESSION['SelectedProductionOrder'] = $_GET['SelectedProductionOrderForDelete'];
		
		$sql10 = "DELETE FROM productionorders WHERE productionorderno = '". $_GET['SelectedProductionOrderForDelete'] ."'";
		$result10 = DB_query($sql10); 					
		
		//Select delivery date of sales order
		$sql0 = "SELECT deliverydate FROM salesorders WHERE orderno = '".$_SESSION['SalesOrderForProduction']."'"; 
		$result0 = DB_query($sql0);		
		$myrow0 = DB_fetch_array($result0);		
		$_POST['SODeliveryDate'] = $myrow0[0];		
		
		//Select list of items in the sales order
		$sql1 = "SELECT stkcode FROM salesorderdetails WHERE orderno = '".$_SESSION['SalesOrderForProduction']."'"; 
		$result1 = DB_query($sql1);
			
		//Select transportation modes
		$sql8 = "SELECT * FROM transportationmodes"; 
		$result8 = DB_query($sql8);		

		$msg = "Production order ". $_GET['SelectedProductionOrderForDelete'] ." deleted successfully";
		prnMsg($msg,'success');		
	}
	
	//If Edit link is clicked
	if(isset($_GET['SelectedProductionOrderForEdit']))
	{		
		$sql11 = "SELECT status FROM productionorders WHERE productionorderno = '". $_GET['SelectedProductionOrderForEdit'] ."'";
		$result11 = DB_query($sql11);
		$myrow11 = DB_fetch_array($result11);		
		
		$_SESSION['Status'] = 1;
		$_POST['SONumber'] = $_SESSION['SalesOrderForProduction'];
		$_SESSION['SelectedProductionOrder'] = $_GET['SelectedProductionOrderForEdit'];
			
		//Select delivery date of sales order
		$sql0 = "SELECT deliverydate FROM salesorders WHERE orderno = '".$_SESSION['SalesOrderForProduction']."'"; 
		$result0 = DB_query($sql0);		
		$myrow0 = DB_fetch_array($result0);		
		$_POST['SODeliveryDate'] = $myrow0[0];
			
		//Select list of items in the sales order
		$sql1 = "SELECT stkcode FROM salesorderdetails WHERE orderno = '".$_SESSION['SalesOrderForProduction']."'"; 
		$result1 = DB_query($sql1);
							
		//Select details against selected production order number
		$sql6 = "SELECT item, noofitems, transport, deliverydate FROM productionorders WHERE productionorderno = '".$_SESSION['SelectedProductionOrder']."'";
		$result6 = DB_query($sql6);		
		$myrow6 = DB_fetch_array($result6);
			
		//Select transportation mode of selected production order number 
		$sql9 = "SELECT transportid, transportname FROM transportationmodes WHERE transportid = '".$myrow6[2]."'";
		$result9 = DB_query($sql9);
		$myrow9 = DB_fetch_array($result9);
			
		$_POST['Items'] = $myrow6[0];
		$_POST['POItemNumber'] = $myrow6[1];
		$_POST['PODeliveryDate'] = date('d-m-Y', strtotime($myrow6[3])); //Change sql date format to dd/mm/yyyy				
	}
		
	//If Items drop down changes
	if(isset($_POST['Items'])) 
	{
		if(isset($_POST['Items']) != '0')
		{
			//Select quantity of selected item in the sales order
			$sql2 = "SELECT quantity FROM salesorderdetails WHERE orderno = '".$_POST['SONumber']."' AND stkcode = '".$_POST['Items']."'";
			$result2 = DB_query($sql2);
			$numrows2 = DB_num_rows($result2);
			$myrow2 = DB_fetch_array($result2);			
			$_POST['SOItemNumber'] = $myrow2[0];
			
			if($_SESSION['Status'] == 1)
			{
				//Select quantity of item already assigned to prodution order
				$sql4 = "SELECT SUM(noofitems) AS Total FROM productionorders WHERE salesorderno = '".$_POST['SONumber']."' AND item = '".$_POST['Items']."' AND productionorderno <> '".$_SESSION['SelectedProductionOrder']."'";
				$result4 = DB_query($sql4);
				$numrows4 = DB_num_rows($result4);
				$myrow4 = DB_fetch_array($result4);
				
				if($numrows4 > 0)
				{
					$_POST['RemainingItems'] = $myrow2[0] - $myrow4[0]; //Calculate remaining quantity of items for production order								
				}				
			}
			else
			{			
				//Select quantity of item already assigned to prodution order
				$sql4 = "SELECT SUM(noofitems) AS Total FROM productionorders WHERE salesorderno = '".$_POST['SONumber']."' AND item = '".$_POST['Items']."'";
				$result4 = DB_query($sql4);
				$numrows4 = DB_num_rows($result4);
				$myrow4 = DB_fetch_array($result4);
				
				if($numrows4 > 0)
				{
					$_POST['RemainingItems'] = $myrow2[0] - $myrow4[0]; //Calculate remaining quantity of items for production order								
				}
			}
			$SelectedItem = $_POST['Items'];
		}
		else
		{
			unset($_POST['SOItemNumber']);
			unset($_POST['RemainingItems']);
		}
		
		//Re-populate Items drop-down as it will be null on change
		$sql1 = "SELECT stkcode FROM salesorderdetails WHERE orderno = '".$_SESSION['SalesOrderForProduction']."'"; 
		$result1 = DB_query($sql1);	
				
		$sql8 = "SELECT * FROM transportationmodes"; 
		$result8 = DB_query($sql8);	
		$numrows8 = DB_num_rows($result8);		

	}
	
	//If production manager is adding a new production order insert it into database 
	if(isset($_POST['AddToProductionOrder'])) 
	{	
		//Validations before inserting data		
		if($_POST['POItemNumber'] == '')
		{
			prnMsg( _('Please enter no.of items'),'warn');				
		}
		elseif($_POST['POItemNumber'] == '0')
		{
			prnMsg( _('Qty must be greater than 0'),'warn');				
		}
		elseif($_POST['PODeliveryDate'] == '')
		{
			prnMsg( _('Please select delivery date'),'warn');				
		}
		elseif($_POST['TransportMode'] == '')
		{
			prnMsg( _('Please choose transportion mode'),'warn');				
		}
		elseif($_POST['RemainingItems'] == '0')
		{
			prnMsg( _('There are no items '.$_POST['Items'].' remaining'),'warn');
		}
		elseif($_POST['POItemNumber']> $_POST['RemainingItems'])
		{
			prnMsg( _('Qty cannot be greater than Remaining Qty'),'warn');
		}
		else
		{			
			$PODeliveryDate = FormatDateforSQL($_POST['PODeliveryDate']); //Convert date into sql database format
			
			if($_SESSION['Status'] == 1)//If it is editing existing entry
			{
				$sql5 = "UPDATE productionorders SET productionorderno = '". $_SESSION['SelectedProductionOrder'] ."', salesorderno = '". $_POST['SONumber'] ."', item = '". $_POST['Items'] ."', noofitems = '". $_POST['POItemNumber'] ."', transport = '". $_POST['TransportMode'] ."', deliverydate = '". $PODeliveryDate ."', status = '0' WHERE productionorderno = '". $_SESSION['SelectedProductionOrder'] ."'";
				$result5 = DB_query($sql5);	

				$msg = "Production order ". $_SESSION['SelectedProductionOrder'] ." updated successfully";
				prnMsg($msg,'success');						
			}
			else //If it is new entry
			{				
				$PONumber = GetNextTransNo(51, $db);	

				$sql14 = "SELECT * FROM salesorders WHERE orderno = '". $_POST['SONumber'] ."'";
				$result14 = DB_query($sql14);
				$myrow14 = DB_fetch_array($result14);
				
				$sql5 = "INSERT INTO productionorders VALUES ('". $PONumber ."','". $_POST['SONumber'] ."','". $_POST['Items'] ."','". $_POST['POItemNumber'] ."','". $_POST['TransportMode'] ."','". $PODeliveryDate ."', '0','".$myrow14[5]."','".$myrow14[1]."','".$myrow14[2]."','".$myrow14[3]."','".$myrow14[4]."','".$myrow14[6]."','".$myrow14[7]."','".$myrow14[8]."','".$myrow14[9]."','".$myrow14[10]."','".$myrow14[11]."','".$myrow14[12]."','".$myrow14[13]."','".$myrow14[14]."','".$myrow14[15]."','".$myrow14[16]."','".$myrow14[17]."','".$myrow14[18]."','".$myrow14[19]."','".$myrow14[20]."','".$myrow14[22]."','".$myrow14[23]."','".$myrow14[24]."','".$myrow14[25]."','".$myrow14[26]."','".$myrow14[27]."','".$myrow14[28]."','".$myrow14[29]."','".$myrow14[30]."','".$myrow14[31]."','".$myrow14[32]."','".$myrow14[33]."','".$myrow14[34]."','".$myrow14[35]."','".$myrow14[36]."','".$myrow14[37]."')";
				$result5 = DB_query($sql5);	
				
				$sql15 = "SELECT * FROM salesorderdetails WHERE orderno = '". $_POST['SONumber'] ."'";
				$result15 = DB_query($sql15);
				$myrow15 = DB_fetch_array($result15);
				
				$POdetailsNumber = GetNextTransNo(53, $db);
				$sql16 = "INSERT INTO productionorderdetails (orderlineno,orderno,stkcode,unitprice,quantity,discountpercent,narrative,itemdue,poline) VALUES('".$POdetailsNumber."','".$PONumber."','".$_POST['Items']."','".$myrow15[4]."','".$_POST['POItemNumber']."','".$myrow15[7]."','".$myrow15[10]."','".$myrow15[11]."','".$myrow15[12]."')";
				$result16 = DB_query($sql16);
			}
			
			$RemainingItems = $_POST['RemainingItems'] - $_POST['POItemNumber'];							
			$_POST['RemainingItems'] = $RemainingItems; //Calculate remaining quantity of items for production order			
		}	
		
		unset($_SESSION['Status']);
		unset($_POST['POItemNumber']);
		//unset($_POST['PODeliveryDate']);
		unset($_SESSION['SelectedProductionOrder']);
	}
	
	if($_POST['Cancel'])
	{
		unset($_POST['POItemNumber']);
		unset($_POST['PODeliveryDate']);
		unset($_SESSION['Status']);
	}
	
	echo 	'<form action="', htmlspecialchars($_SERVER['PHP_SELF'], ENT_QUOTES, 'UTF-8'), '" method="post">','<input type="hidden" name="FormID" value="', $_SESSION['FormID'], '" />';
	
	echo 	'<p class="page_title_text">
				<img src="'.$RootPath.'/css/'.$Theme.'/images/customer.png" title="' . _('Production Order') . '" alt="" />' . ' ' . _('Production Order') . '
			</p>';
		  
	echo 	'<table class="selection" cellspacing="4">
				<tbody>
					<tr>
						<td valign="top">
							<table class="selection">
								<tr>
									<td colspan=6>
										<p class="page_title_text">' . _('Sales Order Details') . '</p>
									</td>
								</tr>
								<tr>
									<td></td>									
									<td>' . _('Sales Order Number') . ':</td>
									<td><input type="text" style="background-color:transparent;border:none;font-weight: bold" readonly="true" name="SONumber" value="' . $_POST['SONumber'] . '" size="12" maxlength="12"/></td>
									<td>' . _('Delivery Date') . ':</td>
									<td><input type="text" style="background-color:transparent;border:none;font-weight: bold" readonly="true" name="SODeliveryDate" value="' . $_POST['SODeliveryDate'] . '" size="12" maxlength="12"/></td>									
									<td></td>
								</tr>
								<tr>
									<td>' . _('Items') . ':</td>
									<td>
										<select name="Items" onchange="ReloadForm(HiddenButton)">
											<option selected="selected" value="0"></option>';											
											
												while ($myrow1 = DB_fetch_array($result1)) 
												{
													if ($SelectedItem == $myrow1['stkcode'])
													{
														echo '<option selected="selected" value="'. $myrow1['stkcode'] . '">' . $myrow1['stkcode'] . '</option>';
														unset($SelectedItem);
													} 
													else 
													{
														echo '<option value="'. $myrow1['stkcode'] . '">' . $myrow1['stkcode'] . '</option>';
													}
												} //end while loop
																		
										echo'</select>
									</td>	
									<td>' . _('Qty') . ':</td>
									<td><input type="text" style="background-color:transparent;border:none;font-weight: bold" readonly="true" name="SOItemNumber" value="' . $_POST['SOItemNumber'] . '" size="12" maxlength="12"/></td>
									<td>' . _('Remaining Qty') . ':</td>
									<td><input type="text" style="background-color:transparent;border:none;font-weight: bold" readonly="true" name="RemainingItems" value="' . $_POST['RemainingItems'] . '" size="12" maxlength="12"/></td>
								</tr>
								<tr>
									<td colspan=6>
										<p>
											<input type="submit" style="display:none" name="HiddenButton" value=""/>											
										</p>
									</td>
								</tr>
								<tr>
									<td colspan=6>
										<p class="page_title_text">' . _('Enter Production Order Details') . '</p>
									</td>
								</tr>
								<tr>							
									<td>' . _('Qty') . ':</td>
									<td><input type="text" name="POItemNumber" value="' . $_POST['POItemNumber'] . '" size="12" maxlength="12"/></td>									
									<td>' . _('Delivery Date') . ':</td>
									<td><input class="date" alt="', $_SESSION['DefaultDateFormat'], '" name="PODeliveryDate" value="' . $_POST['PODeliveryDate'] . '" onchange="isDate(this, this.value, ', "'", $_SESSION['DefaultDateFormat'], "'", ')" size="12" maxlength="12"/></td>
									<td>' . _('Mode of Transport') . ':</td>
									<td>
										<select name="TransportMode">';
																									
											while($myrow8 = DB_fetch_array($result8))
											{
												if($_SESSION['Status']==1)//If edit link is clicked
												{
													if($myrow9['transportid'] == $myrow8['transportid'])
													{
														echo	'<option selected="selected" value="'. $myrow8['transportid'] .'">' . $myrow8['transportname'] . '</option>';
													}													
													else
													{
														echo	'<option value="'. $myrow8['transportid'] .'">' . $myrow8['transportname'] . '</option>';	
													}	
												}
												else
												{
													echo	'<option value="'. $myrow8['transportid'] .'">' . $myrow8['transportname'] . '</option>';	
												}												
											}																					
																												
										echo '</select>
									</td>
								</tr>
								<tr>
									<td colspan=6>
										<p></p>
									</td>
								</tr>
								<tr>
									<td colspan=2>
									</td>';
									
										if($_SESSION['Status'] == 1)
										{
											echo '<td>
													<input type="submit" name="AddToProductionOrder" value="' . _('Update Production Order') . '" />
												 </td>
												 <td>
													<input type="submit" name="Cancel" value="' . _('Cancel') . '" />
												 </td>';
										}
										else
										{
											echo '<td colspan=2>												
													<input type="submit" name="AddToProductionOrder" value="' . _('Add to Production Order') . '" />';
										}
									echo '</td>
									<td colspan=2>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>';	
			
			
	if(isset($_GET['OrderNumber']) OR isset($_POST['AddToProductionOrder']) OR isset($_POST['Items']) OR $_SESSION['Status'] == 2)
	{	
		if($_SESSION['Status'] == 2)
		{
			unset($_SESSION['Status']);
		}
		
		$sql3 = "SELECT * FROM productionorders WHERE salesorderno = '".$_SESSION['SalesOrderForProduction']."'"; //Selecting previous production orders created for this sales order if any
		$result3 = DB_query($sql3);
		$numrows3 = DB_num_rows($result3);		
		
		if($numrows3>0)
		{
			echo  	'<table cellpadding="2" class="selection" rules="all" id="POTable">
						<thead>			
							<tr>
								<th class="ascending">' . _('Sales Order No') . '</th>
								<th class="ascending">' . _('Production Order No') . '</th>
								<th class="ascending">' . _('Invoice') . '</th>
								<th class="ascending">' . _('Item') . '</th>
								<th class="ascending">' . _('Qty') . '</th>
								<th class="ascending">' . _('Qty Left') . '</th>
								<th class="ascending">' . _('Mode of Transport') . '</th>
								<th class="ascending">' . _('Delivery Date') . '</th>
								<th class="ascending"></th>
								<th class="ascending"></th>	
								<th class="ascending"></th>	
							</tr>
						</thead>';
		}				
	}
		
	echo '<tbody>';
	
	//Loop through production orders
	while (($myrow3 = DB_fetch_array($result3))) 
	{	
		$sql12 = "SELECT mbflag FROM stockmaster WHERE stockid = '". $myrow3['item'] ."'";
		$result12 = DB_query($sql12);
		$myrow12 = DB_fetch_array($result12);
		
		if($myrow12[0] == 'M')
		{
			$sql13 = "SELECT SUM(noofitems) AS Total FROM assignproductionline WHERE productionorderno='". $myrow3['productionorderno'] ."'";
			$result13 = DB_query($sql13);
			$myrow13 = DB_fetch_array($result13); //Calculate total no.of items in work order or production line
			
			$leftitems = $myrow3['noofitems'] - $myrow13['Total']; //Calculate no of items left to be assigned to work order or production line
			$argument = $myrow3['productionorderno']." ".$leftitems; //Combining production order number and no.of items in each table row to be passed as a single argument
			
			$sql9 = "SELECT transportid, transportname FROM transportationmodes WHERE transportid = '".$myrow3['transport']."'";
			$result9 = DB_query($sql9);
			$myrow9 = DB_fetch_array($result9);
			
			$sql11 = "SELECT status FROM productionorders WHERE productionorderno = '". $myrow3['productionorderno'] ."'";
			$result11 = DB_query($sql11);
			$myrow11 = DB_fetch_array($result11);
			
			$Confirm_Invoice = $RootPath . '/ConfirmDispatch_Invoice.php?ProOrderNumber=' . $myrow3['productionorderno'];
			
			$_SESSION['NoOfItems'] = $myrow3['noofitems'];
			
			if($myrow11[0] == 2)
			{
				$workorderstatus = "Work Order Completed";
			}
			elseif($myrow11[0] == 1)
			{
				$workorderstatus = "In Work Order";
			}
				
			if($myrow11[0] != 0 AND $leftitems > '0')
			{
				printf('<tr>
					<td>%s</td>
					<td>%s</td>
					<td><a href="%s">' . _('Invoice') . '</a></td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td colspan=2 style="text-align:center">%s</td>	
					<td><button type="submit" name="PLAssign" value="'.$argument.'" >'. _('Assign Work Order') . '</button></td>					
					</tr>',
					$myrow3['salesorderno'],
					$myrow3['productionorderno'],
					$Confirm_Invoice,
					$myrow3['item'],
					$myrow3['noofitems'],
					$leftitems,
					$myrow9['transportname'],
					$myrow3['deliverydate'],
					$workorderstatus
				);
			}					
			elseif($myrow11[0] != 0)
			{
				printf('<tr>
					<td>%s</td>
					<td>%s</td>
					<td><a href="%s">' . _('Invoice') . '</a></td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td colspan=3 style="text-align:center">%s</td>				
					</tr>',
					$myrow3['salesorderno'],
					$myrow3['productionorderno'],
					$Confirm_Invoice,
					$myrow3['item'],
					$myrow3['noofitems'],
					$leftitems,
					$myrow9['transportname'],
					$myrow3['deliverydate'],
					$workorderstatus
				);
			}	
			else
			{
				printf('<tr>
					<td>%s</td>
					<td>%s</td>
					<td><a href="%s">' . _('Invoice') . '</a></td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td><a href="/Hexagon/CreateProductionOrder.php?&amp;SelectedProductionOrderForEdit=%s\">' . _('Edit') . '</a></td>
					<td><a href="/Hexagon/CreateProductionOrder.php?&amp;SelectedProductionOrderForDelete=%s&amp;delete=1\" onclick=\"return confirm(' . _('Are you sure you wish to delete?') . ');\">' . _('Delete') . '</a></td>
					<td><button type="submit" name="PLAssign" value="'.$argument.'" >'. _('Assign Line Order') . '</button></td>
					</tr>',
					$myrow3['salesorderno'],
					$myrow3['productionorderno'],
					$Confirm_Invoice,
					$myrow3['item'],
					$myrow3['noofitems'],
					$leftitems,
					$myrow9['transportname'],
					$myrow3['deliverydate'],				
					$myrow3['productionorderno'],
					$myrow3['productionorderno']				
				);
			}
		}		
	}
		
	//If production order is being inserted to work order
	if(isset($_POST['PLAssign']))
	{			
		echo '<meta http-equiv="Refresh" content="0; url=' . $RootPath . '/AssignProductionLine.php?ProductionOrder=' . $_POST['PLAssign'] . '">';
	}
	
	echo '</tbody>';
	echo '</table>';
			
	echo '</form>';
	
	include('includes/footer.inc');			

?>

<script src="DataTables/media/js/jquery.js" type="text/javascript" ></script>
<script src="DataTables/media/js/jquery.dataTables.js" type="text/javascript"> </script>

<style type="text/css">
	@import "DataTables/media/css/jquery.dataTables.css";
	table.dataTable 
	{
		width: 50%;
		margin: 0 auto;
		clear: both;
		border-collapse: separate;
		border-spacing: 0;
	}

	.dataTables_wrapper .dataTables_filter 
	{
		float: left;
		text-align: right;
		padding-left: 607px;
		padding-bottom: 10px;
	}
	.dataTables_wrapper .dataTables_length 
	{
		float: left;
		padding-left: 200px;
	}
</style>

<script type="text/javascript" charset="uts-8">
	$(document).ready(function ()
	{
		$("#POTable").dataTable(
		{
			"destroy":true
		});
	}); 
</script>


