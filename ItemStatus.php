<?php
	
	include('includes/session.inc');
	$Title = _('Item Status');
	include('includes/header.inc');

	if(isset($_POST['submit']))
	{
		echo '<meta http-equiv="Refresh" content="0; url=' . $RootPath .'/Stocks.php?TempCust=' . $_POST['SampProd']  . '">';
	}
	
	echo '<form action="', htmlspecialchars($_SERVER['PHP_SELF'], ENT_QUOTES, 'UTF-8'), '" method="post">','<input type="hidden" name="FormID" value="', $_SESSION['FormID'], '" />';
	
	echo '<p class="page_title_text">
			<img src="'.$RootPath.'/css/'.$Theme.'/images/customer.png" title="' . _('Item') . '" alt="" />' . ' ' . _('Sample Product') . '
		  </p>';
		  
	echo 	'<table class="selection" cellspacing="4">
				<tbody>
					<tr>
						<td valign="top">
							<table class="selection">
								<tr>
									<td>' . _('Sample Product') . ':</td>
									<td>
										<select tabindex="1" name="SampProd" required="required">';
									
									if($_POST['SampProd']==0)
									{
										echo	'<option selected="selected" value="0">' . _('No') . '</option>
										<option value="1">' . _('Yes') . '</option>';
									}
									elseif($_POST['SampProd']==1)
									{
										echo	'<option value="0">' . _('No') . '</option>
										<option selected="selected" value="1">' . _('Yes') . '</option>';
									}	
									else
									{								
										echo	'<option selected="selected" value="0">' . _('No') . '</option>
										<option value="1">' . _('Yes') . '</option>
										</select>';
									}
									
									echo	'</td>
									<td>
										<input tabindex="2" type="submit" name="submit" value="' . _('Enter Item Details') . '" />
									</td>
								</tr>
							</table>
						</td>
					</tr>					
				</tbody>
			</table>';	
			
	echo '</form>';
	
	include('includes/footer.inc');			

?>