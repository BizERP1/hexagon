<?php

	/* $Id: ProductionLine.php 6998 2014-11-22 02:28:56Z daintree $*/

	include('includes/session.inc');
	$Title = _('Production Lines');
	include('includes/header.inc');

	if (isset($_POST['SelectedType']))
	{
		$SelectedType = mb_strtoupper($_POST['SelectedType']);
	} 
	elseif (isset($_GET['SelectedType']))
	{
		$SelectedType = mb_strtoupper($_GET['SelectedType']);
	}

	if (isset($Errors)) 
	{
		unset($Errors);
	}

	$Errors = array();

	echo '<p class="page_title_text"><img src="'.$RootPath.'/css/'.$Theme.'/images/maintenance.png" title="' . _('Search') . '" alt="" />' . ' ' . $Title . '</p>';

	if (isset($_POST['submit'])) 
	{
		//initialise no input errors assumed initially before we test
		$InputError = 0;

		/* actions to take once the user has clicked the submit button ie the page has called itself with some user input */

		//first off validate inputs sensible
		$i=1;

		if (mb_strlen($_POST['PLId']) > 2) 
		{
			$InputError = 1;
			prnMsg(_('The Production line id must be two characters or less long'),'error');
			$Errors[$i] = 'ProductionLines';
			$i++;
		}
		elseif ($_POST['PLId']=='' OR $_POST['PLId']==' ' OR $_POST['PLId']=='  ') 
		{
			$InputError = 1;
			prnMsg( _('The production line cannot be an empty string or spaces'),'error');
			$Errors[$i] = 'ProductionLines';
			$i++;
		} 
		elseif( trim($_POST['PLCapacity'])=='')
		{
			$InputError = 1;
			prnMsg (_('The production line capacity cannot be empty'),'error');
			$Errors[$i] = 'ProductionLines';
			$i++;
		} 
		elseif (mb_strlen($_POST['PLCapacity']) >15) 
		{
			$InputError = 1;
			echo prnMsg(_('The production line capacity must be fifteen digits or less long'),'error');
			$Errors[$i] = 'ProductionLines';
			$i++;
		} 
		
		if (isset($SelectedType) AND $InputError !=1) 
		{
			$sql = "UPDATE productionlines SET productionlinecapacity = '" . $_POST['PLCapacity'] . "' WHERE productionlineid = '".$SelectedType."'";
			$msg = _('The product line') . ' ' . $SelectedType . ' ' .  _('has been updated');
		} 
		elseif ( $InputError !=1 ) 
		{
			// First check the type is not being duplicated

			$checkSql = "SELECT count(*) FROM productionlines WHERE productionlineid = '" . $_POST['PLId'] . "'";
			$CheckResult = DB_query($checkSql);
			$CheckRow = DB_fetch_row($CheckResult);

			if ( $CheckRow[0] > 0 ) 
			{
				$InputError = 1;
				prnMsg( _('The procuction line ') . $_POST['PLId'] . _(' already exist.'),'error');
			} 
			else 
			{
				// Add new record on submit
				
				$sql = "INSERT INTO productionlines (productionlineid, productionlinecapacity) VALUES ('" . str_replace(' ', '', $_POST['PLId']) . "','" . $_POST['PLCapacity'] . "')";
				$msg = _('Production line') . ' ' . $_POST['PLCapacity'] .  ' ' . _('has been created');
				$checkSql = "SELECT count(productionlineid) FROM productionlines";
				$result = DB_query($checkSql);
				$row = DB_fetch_row($result);
			}
		}

		if ( $InputError !=1) 
		{
			//run the SQL from either of the above possibilites
			$result = DB_query($sql);

			// Check the default production lines exists
			$checkSql = "SELECT count(*) FROM productionlines WHERE productionlineid = '" . $_SESSION['ProductionLines'] . "'";
			$CheckResult = DB_query($checkSql);
			$CheckRow = DB_fetch_row($CheckResult);

			// If it doesnt then update config with newly created one.
			if ($CheckRow[0] == 0) 
			{
				$sql = "UPDATE config SET confvalue='".$_POST['PLId']."' WHERE confname='ProductionLines'";
				$result = DB_query($sql);
				$_SESSION['ProductionLines'] = $_POST['PLId'];
			}

			prnMsg($msg,'success');

			unset($SelectedType);
			unset($_POST['PLId']);
			unset($_POST['PLCapacity']);
		}
	} 
	elseif ( isset($_GET['delete']) ) 
	{
		// PREVENT DELETES IF DEPENDENT RECORDS IN 'DebtorTrans'
		// Prevent delete if saletype exist in customer transactions

		$sql= "SELECT COUNT(*) FROM workorders WHERE productionline = '".$SelectedType."'";		
		$result = DB_query($sql);
		$myrow = DB_fetch_row($result);
		
		if ($myrow[0]>0) 
		{
			prnMsg(_('Cannot delete this production line because work orders have been created using this production line') . '<br />' . _('There are') . ' ' . $myrow[0] . ' ' . _('work orders using this production line'),'error');

		} 
		else 
		{			
			$sql="DELETE FROM productionlines WHERE productionlineid ='" . $SelectedType . "'";
			$ErrMsg = _('The production line could not be deleted because');
			$result = DB_query($sql,$ErrMsg);
			prnMsg(_('Production Line') . ' ' . $SelectedType  . ' ' . _('has been deleted') ,'success');
				
			unset ($SelectedType);
			unset($_GET['delete']);
			
		} 
	}


	if(isset($_POST['Cancel']))
	{
		unset($SelectedType);
		unset($_POST['PLId']);
		unset($_POST['PLCapacity']);
	}

	if (!isset($SelectedType))
	{
		/* It could still be the second time the page has been run and a record has been selected for modification - SelectedType will exist because it was sent with the new call. If its the first time the page has been displayed with no parameters
		then none of the above are true and the list of production lines will be displayed with
		links to delete or edit each. These will call the same page again and allow update/input
		or deletion of the records*/

		$sql = "SELECT productionlineid,productionlinecapacity FROM productionlines ORDER BY productionlineid";
		$result = DB_query($sql);

		echo '<table class="selection">
			<tr>
					<th class="ascending">' . _('Production Line Code') . '</th>
					<th class="ascending">' . _('Production Line Capacity') . '</th>
			</tr>';

		$k=0; //row colour counter

		while ($myrow = DB_fetch_row($result)) 
		{
			if ($k==1)
			{
				echo '<tr class="EvenTableRows">';
				$k=0;
			} 
			else 
			{
				echo '<tr class="OddTableRows">';
				$k=1;
			}

			printf('<td>%s</td>
			<td>%s</td>
			<td><a href="%sSelectedType=%s">' . _('Edit') . '</a></td>
			<td><a href="%sSelectedType=%s&amp;delete=yes" onclick="return confirm(\'' . _('Are you sure you wish to delete this production line ?') . '\');">' . _('Delete') . '</a></td>
			</tr>',
			$myrow[0],
			$myrow[1],
			htmlspecialchars($_SERVER['PHP_SELF'],ENT_QUOTES,'UTF-8') . '?', $myrow[0],
			htmlspecialchars($_SERVER['PHP_SELF'],ENT_QUOTES,'UTF-8') . '?', $myrow[0]);
		}
		//END WHILE LIST LOOP
		echo '</table>';
	}

	//end of ifs and buts!
	if (isset($SelectedType)) 
	{
		echo '<br />
				<div class="centre">
					<a href="' . htmlspecialchars($_SERVER['PHP_SELF'],ENT_QUOTES,'UTF-8') .'">' . _('Show All Production Lines Defined') . '</a>
				</div>';
	}
	if (! isset($_GET['delete']))
	{
		echo '<form method="post" action="' . htmlspecialchars($_SERVER['PHP_SELF'],ENT_QUOTES,'UTF-8') . '" >
				<div>
					<input type="hidden" name="FormID" value="' . $_SESSION['FormID'] . '" />
				<br />';


		// The user wish to EDIT an existing type
		if ( isset($SelectedType) AND $SelectedType!='' ) 
		{
			$sql = "SELECT productionlineid, productionlinecapacity FROM productionlines WHERE productionlineid='" . $SelectedType . "'";
			$result = DB_query($sql);
			$myrow = DB_fetch_array($result);

			$_POST['PLId'] = $myrow['productionlineid'];
			$_POST['PLCapacity']  = $myrow['productionlinecapacity'];

			echo '<input type="hidden" name="SelectedType" value="' . $SelectedType . '" />
				  <input type="hidden" name="PLId" value="' . $_POST['PLId'] . '" />
						<table class="selection">
						<tr>
						<th colspan="4"><b>' . _('Production Line') . '</b></th>
					</tr>
					<tr>
						<td>' . _('Production Line Id') . ':</td>
						<td>' . $_POST['PLId'] . '</td>
					</tr>';

		} 
		else 	
		{
			// This is a new type so the user may volunteer a type code

			echo '<table class="selection">
					<tr>
						<th colspan="4"><b>' . _('Production Line') . '</b></th>
					</tr>
					<tr>
						<td>' . _('Production Line Id') . ':</td>
						<td><input type="text" ' . (in_array('ProductionLine',$Errors) ? 'class="inputerror"' : '' ) .' size="3" maxlength="2" name="PLId" /></td>
					</tr>';
		}

		if (!isset($_POST['PLCapacity'])) 
		{
			$_POST['PLCapacity']='';
		}
		
		echo '<tr>
				<td>' . _('Capacity') . ':</td>
				<td><input type="text" name="PLCapacity" value="' . $_POST['PLCapacity'] . '" /></td>
			</tr>
			</table>'; // close main table

		echo '<br /><div class="centre"><input type="submit" name="submit" value="' . _('Accept') . '" /><input type="submit" name="Cancel" value="' . _('Cancel') . '" /></div>
				</div>
			  </form>';

	} // end if user wish to delete

	include('includes/footer.inc');
?>