<?php
	
	$dataTable=1;
	
	include('includes/SQL_CommonFunctions.inc');
	include('includes/session.inc');
	$Title = _('Production Orders');	
	include('includes/header.inc');
	
	
	if($_POST['InsertWorkOrder'])
	{
			$SQL = "SELECT SUM(quantity) FROM locstock WHERE stockid='" . $_POST['Item'] . "'";
			$QOHResult = DB_query($SQL);
			$QOHRow = DB_fetch_row($QOHResult);
			$QOH = $QOHRow[0];

			$SQL = "SELECT SUM(productionorderdetails.quantity - productionorderdetails.qtyinvoiced) AS qtydemand
					FROM productionorderdetails INNER JOIN productionorders
					ON productionorderdetails.orderno=productionorders.productionorderno
					WHERE productionorderdetails.stkcode = '" . $_POST['Item'] . "'
					AND productionorderdetails.completed = 0
					AND productionorders.quotation=0";
			$DemandResult = DB_query($SQL);
			$DemandRow = DB_fetch_row($DemandResult);
			$QuantityDemand = $DemandRow[0];

			$SQL = "SELECT SUM((productionorderdetails.quantity-productionorderdetails.qtyinvoiced)*bom.quantity) AS dem
					FROM productionorderdetails INNER JOIN productionorders
					ON productionorderdetails.orderno=productionorders.productionorderno
					INNER JOIN bom ON productionorderdetails.stkcode=bom.parent
					INNER JOIN stockmaster ON stockmaster.stockid=bom.parent
					WHERE productionorderdetails.quantity-productionorderdetails.qtyinvoiced > 0
					AND bom.component='" . $_POST['Item'] . "'
					AND productionorders.quotation=0
					AND stockmaster.mbflag='A'
					AND productionorderdetails.completed=0";
			$AssemblyDemandResult = DB_query($SQL);
			$AssemblyDemandRow = DB_fetch_row($AssemblyDemandResult);
			$QuantityAssemblyDemand = $AssemblyDemandRow[0];

			$SQL = "SELECT SUM(purchorderdetails.quantityord - purchorderdetails.quantityrecd) as qtyonorder
					FROM purchorderdetails,purchorders
					WHERE purchorderdetails.orderno = purchorders.orderno
					AND purchorderdetails.itemcode = '" . $_POST['Item'] . "'
					AND purchorderdetails.completed = 0";
			$PurchOrdersResult = DB_query($SQL);
			$PurchOrdersRow = DB_fetch_row($PurchOrdersResult);
			$QuantityPurchOrders = $PurchOrdersRow[0];

			$SQL = "SELECT SUM(woitems.qtyreqd - woitems.qtyrecd) as qtyonorder
					FROM woitems INNER JOIN workorders
					ON woitems.wo=workorders.wo
					WHERE woitems.stockid = '" . $_POST['Item'] . "'
					AND woitems.qtyreqd > woitems.qtyrecd
					AND workorders.closed = 0";
			$WorkOrdersResult = DB_query($SQL);
			$WorkOrdersRow = DB_fetch_row($WorkOrdersResult);
			$QuantityWorkOrders = $WorkOrdersRow[0];
	
			$SQL = "SELECT eoq,description,units,controlled,serialised FROM stockmaster WHERE stockid = '". $_POST['Item'] ."'";
			$StockDetailsResult = DB_query($SQL);
			$Stock = DB_fetch_array($StockDetailsResult);
			
			$ShortfallQuantity = $QOH-$QuantityDemand-$QuantityAssemblyDemand+$QuantityPurchOrders+$QuantityWorkOrders; 

			
			if($ShortfallQuantity < 0) 
			{
				if($ShortfallQuantity + $Stock['eoq'] < 0) 
				{
					$WOQuantity = -$ShortfallQuantity;
				} 
				else 
				{
					$WOQuantity = $Stock['eoq'];
				}

				$PLStartDate = FormatDateforSQL($_POST['Sdate']); //Convert date into sql database format
							
				$sql9 = "SELECT productionlinecapacity FROM productionlines WHERE productionlineid = '". $_POST['PL'] ."'";
				$result9 = DB_query($sql9);
				$myrow9 = DB_fetch_array($result9);
							
				$quo = $WOQuantity/$myrow9[0];
				$rem = $WOQuantity%$myrow9[0];
				$quo = floor($quo);				
				$date = date('Y');
				
				$noofwo = $quo;
				if($rem > 0)
				{
					$noofwo++;
				}
				
				$quo1 = $noofwo/$_POST['Batch'];
				$rem1 = $noofwo%$_POST['Batch'];
				
				$remitems = $WOQuantity;
				
				$j = 1;
				while($j <= $_POST['Batch'])
				{	
					$banumber = GetNextTransNo(52,$db);
					$batch1 = str_pad($banumber, 4, '0', STR_PAD_LEFT);
					
					if($j == '1')
					{
						$n = $quo1+$rem1;					
					}
					else
					{
						$n = $quo1;					
					}			
					
					if($j == '1')
					{
						$intpart = $myrow9[0];
						$noofitems = $intpart;
					}	
					
													
					$i = 1;	
					
											
					while($i <= $n)
					{	
						if($remitems<$noofitems)
						{
							$noofitems = $remitems;
						}				
						$wonumber = GetNextTransNo(40,$db);				
						$batch2 = str_pad($i, 4, '0', STR_PAD_LEFT);
						$batchanalysis = "HNEPL/". $batch1 ."/". $date;
						$batchnumber = "HNEPL/". $batch1 ."/". $batch2 ."/". $date;
						
						$sql11 = "INSERT INTO assignproductionline VALUES ('". $wonumber ."','". $_POST['PO'] ."','". $batchnumber ."','". $batchanalysis ."','". $_POST['Item'] ."','". $noofitems ."','". $_POST['PL'] ."','". $PLStartDate ."','". $_POST['Ddate'] ."','0')";
						$result11 = DB_query($sql11);
													
						$sql8 = "INSERT INTO workorders (wo, loccode, requiredby, startdate) VALUES ('" . $wonumber . "', '" . $_SESSION['Location'] . "', '" . $_POST['Ddate'] . "', '" . $PLStartDate . "')";
						$result8 = DB_query($sql8);
						
						$CostResult = DB_query("SELECT SUM((materialcost+labourcost+overheadcost)*bom.quantity) AS cost
																FROM stockmaster INNER JOIN bom
																ON stockmaster.stockid=bom.component
																WHERE bom.parent='" . $_POST['Item'] . "'
																AND bom.loccode='" . $_SESSION['Location'] . "'");
						$CostRow = DB_fetch_row($CostResult);
							
						if(is_null($CostRow[0]) OR $CostRow[0]==0) 
						{
							$Cost =0;
							prnMsg(_('In automatically creating a work order for') . ' ' . $_POST['Item'] . ' ' . _('an item on this sales order, the cost of this item as accumulated from the sum of the component costs is nil. This could be because there is no bill of material set up ... you may wish to double check this'),'warn');
						} 
						else 
						{
							$Cost = $CostRow[0];
						}

						$sql7 = "INSERT INTO woitems (wo, stockid, qtyreqd, stdcost) VALUES ('" . $wonumber . "', '" . $_POST['Item'] . "', '" . $noofitems . "', '0')";
						$result7 = DB_query($sql7);

						WoRealRequirements($db, $wonumber, $_SESSION['Location'], $_POST['Item']);
						
						if($Stock[4] == '1') 
						{							
							for ($k=1;$k<=$noofitems;$k++) 
							{
								$sql10 = "INSERT INTO woserialnos (stockid, wo, qualitytext, quantity, serialno) VALUES ('" . $_POST['Item'] . "', '" . $wonumber . "', '', '" . $noofitems .  "', '" . $k . "')";			
								$result10 = DB_query($sql10);								
							}									
						}
						elseif($Stock[3] == '1')
						{
							$sql10 = "INSERT INTO woserialnos (stockid, wo, qualitytext, quantity, serialno) VALUES ('" . $_POST['Item'] . "', '" . $wonumber . "', '', '" . $noofitems .  "', '" . $batchnumber . "')";			
							$result10 = DB_query($sql10);	
						}	
						$i++;

						if($remitems>$noofitems)
						{
							$remitems = $remitems - $noofitems;
						}															
					}
								
					$sql12 = "UPDATE productionorders SET status = '1' WHERE productionorderno = '". $_POST['PO'] ."'";
					$result12 = DB_query($sql12);						
					
					$j++;
				}					
			}
	}
	
	if($_POST['SelectProductionOrder'])
	{
		if($_POST['OrderNumber'] != '')
		{
			$sql1 = "SELECT productionorderno,item,noofitems,debtorno,deliverydate FROM productionorders WHERE productionorderno='".$_POST['OrderNumber']."' AND fromstkloc='".$_POST['OrderLocation']."'"; 
			$result1 = DB_query($sql1);
			$numrows1 = DB_num_rows($result1);	
		}	
		else
		{
			$sql1 = "SELECT productionorderno,item,noofitems,debtorno,deliverydate FROM productionorders WHERE fromstkloc='".$_POST['OrderLocation']."'"; 
			$result1 = DB_query($sql1);
			$numrows1 = DB_num_rows($result1);	
		}
		
		$_SESSION['Status1'] = 0;
		$_SESSION['Location'] = $_POST['OrderLocation'];
	}
		
		
	if($_GET['PO'])
	{
		$sql2 = "SELECT a.workorderno,a.item,a.noofitems,a.productionlineid,a.deliverydate,w.closed FROM assignproductionline a INNER JOIN workorders w ON a.workorderno=w.wo WHERE a.productionorderno = '".$_GET['PO']."'"; 
		$result2 = DB_query($sql2);
		$numrows2 = DB_num_rows($result2);
		
		$sql3 = "SELECT SUM(w.qtyrecd) AS Recvd,SUM(w.qtyreqd) AS Total FROM assignproductionline a INNER JOIN woitems w ON a.workorderno=w.wo WHERE a.productionorderno = '".$_GET['PO']."'"; 
		$result3 = DB_query($sql3);
		$myrow3 = DB_fetch_array($result3);
		
		$sql4 = "SELECT COUNT(a.workorderno) FROM assignproductionline a INNER JOIN workorders w ON a.workorderno=w.wo WHERE a.productionorderno = '".$_GET['PO']."'"; 
		$result4 = DB_query($sql4);
		$myrow4 = DB_fetch_array($result4);
		
		$sql5 = "SELECT COUNT(a.workorderno) FROM assignproductionline a INNER JOIN workorders w ON a.workorderno=w.wo WHERE a.productionorderno = '".$_GET['PO']."' AND w.closed = '1'"; 
		$result5 = DB_query($sql5);
		$myrow5 = DB_fetch_array($result5);
		
		$_SESSION['PO'] = $_GET['PO'];
		$_POST['PO'] = $_GET['PO'];
		$_SESSION['Status1'] = 1;
	}
	else
	{
		$_SESSION['Status1'] = 0;
	}
	
	
	if($_POST['AdditionalWorkOrder'])
	{
		echo '<meta http-equiv="Refresh" content="0; url=' . $RootPath . '/SelectProductionOrders.php?AddWO=' . $_POST['AdditionalWorkOrder'] . '">';
	}
	
	if($_GET['AddWO'])
	{
		$argument = explode(" ",$_GET['AddWO']); 
		$_SESSION['POForWO'] = $argument[0]; 
		$_SESSION['Qty'] = $argument[1];
		$_SESSION['Status1'] = 2;
		
		$sql6 = "SELECT * FROM assignproductionline WHERE productionorderno='". $_SESSION['POForWO'] ."'";
		$result6 = DB_query($sql6);
		$myrow6 = DB_fetch_array($result6);
		
		$_POST['PO'] = $_SESSION['POForWO'];
		$_POST['PL'] = $myrow6['productionlineid'];
		$_POST['Item'] = $myrow6['item'];
		$_POST['Qty'] = $_SESSION['Qty'];
		$_POST['Ddate'] = $myrow6['deliverydate'];
		$_POST['Sdate'] = date('d-m-Y');
	}
	
	echo 	'<form action="', htmlspecialchars($_SERVER['PHP_SELF'], ENT_QUOTES, 'UTF-8'), '" method="post">','<input type="hidden" name="FormID" value="', $_SESSION['FormID'], '" />';
	
	echo 	'<p class="page_title_text">
				<img src="'.$RootPath.'/css/'.$Theme.'/images/customer.png" title="' . _('Production Orders') . '" alt="" />' . ' ' . _('Production Orders') . '
			</p>';
	
	
	echo	'<table class="selection">
				<tr>
					<td colspan=5><p class="page_title_text">' . _('Select Production Order') . '</p></td>
				</tr>
				<tr>
					<td>' . _('Production Order No') . ':</td>
					<td><input type="text" name="OrderNumber" value="' . $_POST['OrderNumber'] . '" size="12" maxlength="12"/></td>
					<td>' . _('From Location') . ':</td>
					<td>
						<select name="OrderLocation">';
							//Select list of locations
							$sql0 = "SELECT loccode, locationname FROM locations"; 
							$result0 = DB_query($sql0);
									
							while($myrow0 = DB_fetch_array($result0))
							{		
								echo	'<option value="'. $myrow0['loccode'] .'">' . $myrow0['locationname'] . '</option>';																								
							}	
											
				echo 	'</select>
					</td>
					<td><input type="submit" name="SelectProductionOrder" value="' . _('Select') . '" /></td>
				</tr>
			</table>';
	
	
	if($_SESSION['Status1'] == 0)
	{
		if($numrows1 > 0)
		{
			echo  	'<table cellpadding="2" class="selection" rules="all" id="POTable">
						<thead>			
							<tr>
								<th class="ascending">' . _('Production Order') . '</th>
								<th class="ascending">' . _('Item') . '</th>	
								<th class="ascending">' . _('Qty') . '</th>									
								<th class="ascending">' . _('Customer') . '</th>
								<th class="ascending">' . _('Delivery Date') . '</th>	
								<th class="ascending">' . _('Invoice') . '</th>								
							</tr>
						</thead>';
		}

		while ($myrow1 = DB_fetch_array($result1)) 
		{
			$Confirm_Invoice = $RootPath . '/ConfirmDispatch_Invoice.php?ProOrderNumber=' . $myrow1['productionorderno'];
			$ModifyPage = $RootPath . '/SelectProductionOrders.php?PO=' . $myrow1['productionorderno'];
			printf('<tr>
						<td><a href="%s">%s</a></td>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>	
						<td>%s</td>
						<td><a href="%s">' . _('Invoice') . '</a></td>						
					</tr>',
					$ModifyPage,
					$myrow1['productionorderno'],					
					$myrow1['item'],
					$myrow1['noofitems'],				
					$myrow1['debtorno'],	
					$myrow1['deliverydate'],
					$Confirm_Invoice
				);		
		}		
	}
	
	if($_SESSION['Status1'] == 1)
	{
		if($numrows2 > 0)
		{
			echo  	'<p class="page_title_text">' . _('Production Order: '.$_POST['PO'].'') . '</p>
					<input type="text" hidden="true" name="PO" value="' . $_POST['PO'] . '"/>
							
					<table cellpadding="2" class="selection" rules="all" id="WOTable">
						<thead>							
							<tr>
								<th class="ascending">' . _('Work Order') . '</th>
								<th class="ascending">' . _('Item') . '</th>	
								<th class="ascending">' . _('Qty') . '</th>									
								<th class="ascending">' . _('Production Line') . '</th>
								<th class="ascending">' . _('Delivery Date') . '</th>								
							</tr>
						</thead>';
		}
		else
		{
				prnMsg( _('No work order defined for this production order'),'warn');	
		}

		while ($myrow2 = DB_fetch_array($result2)) 
		{
			if($myrow2['closed'] == 0)
			{
				$ModifyPage = $RootPath . '/WorkOrderEntry.php?WO=' . $myrow2['workorderno'];
				printf('<tr>
							<td><a href="%s">%s</a></td>
							<td>%s</td>
							<td>%s</td>
							<td>%s</td>	
							<td>%s</td>										
						</tr>',
						$ModifyPage,
						$myrow2['workorderno'],					
						$myrow2['item'],
						$myrow2['noofitems'],				
						$myrow2['productionlineid'],	
						$myrow2['deliverydate']
					);
			}
			else
			{
				printf('<tr>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>	
						<td>%s</td>										
					</tr>',
					$myrow2['workorderno'],										
					$myrow2['item'],
					$myrow2['noofitems'],				
					$myrow2['productionlineid'],	
					$myrow2['deliverydate']
				);	
			}				
		}		
	}
		
	echo 	'</table>';
	
	if($_SESSION['Status1'] == 1)
	{
		if($numrows2 > 0)
		{
			$yield =  ($myrow3['Recvd']/$myrow3['Total']) * 100;
			$qtyleft = $myrow3['Total'] - $myrow3['Recvd'];
			$argument = $_SESSION['PO'] ." ". $qtyleft;
			
			$ModifyPage = $RootPath . '/SelectProductionOrders.php?PO=' . $_SESSION['PO'];
			echo  	'<table cellpadding="2" class="selection" rules="all">
						<thead>			
							<tr>
								<td>' . _('Total Qty Ordered') . '</td>
								<td>' . _('Produced Qty') . '</td>
								<td>' . _('Total Work Orders') . '</td>
								<td>' . _('Open Work Orders') . '</td>
								<td>' . _('Yield %') . '</td>';
								
								if($myrow4[0] == $myrow5[0] AND $yield < 100)
								{
									echo '<td></td>';
								}
					echo	'</tr>
							<tr>
								<td>' . $myrow3['Total'] . '</td>
								<td>' . $myrow3['Recvd'] . '</td>
								<td>' . $myrow4[0] . '</td>
								<td>' . ($myrow4[0] - $myrow5[0]) . '</td>
								<td>' . $yield .'</td>';
								if($myrow4[0] == $myrow5[0] AND $yield < 100)
								{
									echo '<td><button type="submit" name="AdditionalWorkOrder" value="'. $argument .'" >' . _('Generate Additional Work Order') . '</button></td>';
								}
					echo	'</tr>
						</thread>
					</table>';
		}		
	}
	
	if($_SESSION['Status1'] == 2)
	{		
		echo  	'<table cellpadding="2" class="selection" rules="all" id="PLTable">
					<thead>			
						<tr>
							<td>' . _('Production Order') . '</td>
							<td>' . _('Production Line') . '</td>
							<td>' . _('Item') . '</td>
							<td>' . _('Qty') . '</td>					
							<td>' . _('Delivery date') . '</td>
							<td>' . _('Start date') . '</td>
							<td>' . _('No.of Batch Analysis') . '</td>
						</tr>
						<tr>
							<td><input type="text" readonly="true" name="PO" value="' . $_POST['PO'] . '"/></td>
							<td><input type="text" readonly="true" name="PL" value="' . $_POST['PL'] . '"/></td>
							<td><input type="text" readonly="true" name="Item" value="' . $_POST['Item'] . '"/></td>
							<td><input type="text" readonly="true" name="Qty" value="' . $_POST['Qty'] . '"/></td>
							<td><input type="text" readonly="true" name="Ddate" value="' . $_POST['Ddate'] . '"/></td>
							<td><input type="date" name="Sdate" value="' . $_POST['Sdate'] . '"/></td>
							<td><input type="text" name="Batch" value="' . $_POST['Batch'] . '"/></td>
						</tr>
						<tr>
							<td colspan=6 style="text-align:center"><input type="submit" name="InsertWorkOrder" value="' . _('Insert Work Order') . '" /></td>
						</tr>
					</thread>
				</table>';
	}
	
	echo 	'</form>';
	
	include('includes/footer.inc');	
	
?>

<script src="DataTables/media/js/jquery.js" type="text/javascript" ></script>
<script src="DataTables/media/js/jquery.dataTables.js" type="text/javascript"> </script>

<style type="text/css">
	@import "DataTables/media/css/jquery.dataTables.css";
	table.dataTable 
	{
		width: 50%;
		margin: 0 auto;
		clear: both;
		border-collapse: separate;
		border-spacing: 0;
	}

	.dataTables_wrapper .dataTables_filter 
	{
		float: left;
		text-align: right;
		padding-left: 607px;
		padding-bottom: 10px;
	}
	.dataTables_wrapper .dataTables_length 
	{
		float: left;
		padding-left: 200px;
	}
</style>

<script type="text/javascript" charset="uts-8">
	$(document).ready(function ()
	{
		$("#WOTable").dataTable(
		{
			"destroy":true
		});
		
		$("#POTable").dataTable(
		{
			"destroy":true
		});
		
		$("#PLTable").dataTable(
		{
			"destroy":true
		});
		
		$("#QtyTable").dataTable(
		{
			"destroy":true
		});
	}); 
</script>