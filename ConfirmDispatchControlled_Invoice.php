<?php

/* $Id: ConfirmDispatchControlled_Invoice.php 6409 2013-11-18 07:53:20Z exsonqu $*/
$dataTable=1;

include('includes/DefineCartClass.php');
include('includes/DefineSerialItems.php');
include('includes/session.inc');
include('includes/SQL_CommonFunctions.inc');
$Title = _('Specify Dispatched Controlled Items');

/* Session started in header.inc for password checking and authorisation level check */
include('includes/header.inc');

if(isset($_GET['Viewbatch']))
{
	
	if (empty($_GET['identifier'])) 
	{
		/*unique session identifier to ensure that there is no conflict with other order entry sessions on the same machine  */
		$identifier = date('U');
	} 
	else 
	{
		$identifier = $_GET['identifier'];		
	}
		
	echo 	'<br /><a href="'. $RootPath. '/ConfirmDispatch_Invoice.php?identifier=' . $identifier . '">' .  _('Back to Confirmation of Dispatch') . '/' . _('Invoice'). '</a>';

	echo 	'<form action="', htmlspecialchars($_SERVER['PHP_SELF'], ENT_QUOTES, 'UTF-8'), '" method="post">','<input type="hidden" name="FormID" value="', $_SESSION['FormID'], '" />';
	
	if($_GET['Type'] == 1)
	{
		echo 	'<p class="page_title_text">' . _('Serial Nos') . '</p>';
		$sql1 = "SELECT a.workorderno,q.serialno,a.batchanalysisno,a.batchnum FROM qcitems q INNER JOIN assignproductionline a ON q.workorderno=a.workorderno WHERE a.productionorderno='".$_GET['PO']."' AND q.approved='1'";
		$result1 = DB_query($sql1);	
	}
	else
	{
		echo 	'<p class="page_title_text">' . _('Batches') . '</p>';
		$sql1 = "SELECT a.workorderno,a.batchanalysisno,a.batchnum,w.qtyreqd,w.qtyrecd FROM assignproductionline a INNER JOIN woitems w ON a.workorderno=w.wo INNER JOIN qcitems q ON a.workorderno=q.workorderno WHERE a.productionorderno='".$_GET['PO']."' AND q.approved='1'";
		$result1 = DB_query($sql1);	
	}
		
	echo	'<table cellpadding="2" class="selection" rules="all" id="BatchTable">';	
	
	if($_GET['Type'] == 1)
	{
		echo 	'<thead><tr>
					<th class="ascending">' . _('Work Order No.') . '</th>
					<th class="ascending">' . _('Serial No.') . '</th>
					<th class="ascending">' . _('Batch Analysis No.') . '</th>
					<th class="ascending">' . _('Batch No.') . '</th>
				</thead></tr>';	
				
		echo 	'<tbody>';	
		
		while($mynewrow1 = DB_fetch_array($result1))
		{
			echo 	'<tr>
						<td>'.$mynewrow1['workorderno'].'</td>						 
						<td>'.$mynewrow1['serialno'].'</td>
						<td>'.$mynewrow1['batchanalysisno'].'</td>
						<td>'.$mynewrow1['batchnum'].'</td>
					</tr>';
		}
		echo '</tbody>';	
	}
	else
	{
		echo 	'<thead><tr>
					<th class="ascending">' . _('Work Order No.') . '</th>
					<th class="ascending">' . _('Total Qty.') . '</th>
					<th class="ascending">' . _('Recieved Qty.') . '</th>
					<th class="ascending">' . _('Batch Analysis No.') . '</th>
					<th class="ascending">' . _('Batch No.') . '</th>
				</thead></tr>';
		
		echo '<tbody>';	
		
		while($mynewrow1 = DB_fetch_array($result1))
		{
			echo 	'<tr><td>'.$mynewrow1['workorderno'].'</td>
						 <td>'.$mynewrow1['qtyreqd'].'</td>
						 <td>'.$mynewrow1['qtyrecd'].'</td>
						 <td>'.$mynewrow1['batchanalysisno'].'</td>
						 <td>'.$mynewrow1['batchnum'].'</td>
					</tr>';
		}
		echo '</tbody>';	
	}
	
	echo	'</table>';
	echo	'</form>';	
}
else
{

	if (empty($_GET['identifier'])) {
		/*unique session identifier to ensure that there is no conflict with other order entry sessions on the same machine  */
		$identifier=date('U');
	} else {
		$identifier=$_GET['identifier'];
	}

	if (isset($_GET['LineNo'])){
			$LineNo = (int)$_GET['LineNo'];
	} elseif (isset($_POST['LineNo'])){
			$LineNo = (int)$_POST['LineNo'];
	} else {
		echo '<div class="centre">
				<a href="' . $RootPath . '/ConfirmDispatch_Invoice.php">' .  _('Select a line item to invoice') . '</a>
				<br />
				<br />';
		prnMsg( _('This page can only be opened if a line item on a sales order to be invoiced has been selected') . '. ' . _('Please do that first'),'error');
		echo '</div>';
		include('includes/footer.inc');
		exit;
	}

	if (!isset($_SESSION['Items'.$identifier]) OR !isset($_SESSION['ProcessingOrder'])) {
		/* This page can only be called with a sales order number to invoice */
		echo '<div class="centre">
				<a href="' . $RootPath . '/SelectSalesOrder.php">' .  _('Select a sales order to invoice') . '</a>
				<br />';
		prnMsg( _('This page can only be opened if a sales order and line item has been selected Please do that first'),'error');
		echo '</div>';
		include('includes/footer.inc');
		exit;
	}


	/*Save some typing by referring to the line item class object in short form */
	$LineItem = &$_SESSION['Items'.$identifier]->LineItems[$LineNo];


	//Make sure this item is really controlled
	if ( $LineItem->Controlled != 1 ){
		echo '<div class="centre"><a href="' . $RootPath . '/ConfirmDispatch_Invoice.php">' .  _('Back to the Sales Order'). '</a></div>';
		echo '<br />';
		prnMsg( _('The line item must be defined as controlled to require input of the batch numbers or serial numbers being sold'),'error');
		include('includes/footer.inc');
		exit;
	}

	/********************************************
	  Get the page going....
	********************************************/
	echo '<div class="centre">';

	echo '<br /><a href="'. $RootPath. '/ConfirmDispatch_Invoice.php?identifier=' . $identifier . '">' .  _('Back to Confirmation of Dispatch') . '/' . _('Invoice'). '</a>';

	echo '<br /><b>' .  _('Dispatch of up to').' '. locale_number_format($LineItem->Quantity-$LineItem->QtyInv, $LineItem->DecimalPlaces). ' '. _('Controlled items').' ' . $LineItem->StockID  . ' - ' . $LineItem->ItemDescription . ' '. _('on order').' ' . $_SESSION['Items'.$identifier]->OrderNo . ' '. _('to'). ' ' . $_SESSION['Items'.$identifier]->CustomerName . '</b></div>';

	/** vars needed by InputSerialItem : **/
	$StockID = $LineItem->StockID;
	$RecvQty = $LineItem->Quantity-$LineItem->QtyInv;
	$ItemMustExist = true;  /*Can only invoice valid batches/serial numbered items that exist */
	$LocationOut = $_SESSION['Items'.$identifier]->Location;
	$InOutModifier=1;
	$ShowExisting=true;

	include ('includes/InputSerialItems.php');

	/*TotalQuantity set inside this include file from the sum of the bundles
	of the item selected for dispatch */
	$_SESSION['Items'.$identifier]->LineItems[$LineNo]->QtyDispatched = $TotalQuantity;
}
include('includes/footer.inc');
exit;
?>

<script src="DataTables/media/js/jquery.js" type="text/javascript" ></script>
<script src="DataTables/media/js/jquery.dataTables.js" type="text/javascript"> </script>

<style type="text/css">
	@import "DataTables/media/css/jquery.dataTables.css";
	table.dataTable 
	{
		width: 50%;
		margin: 0 auto;
		clear: both;
		border-collapse: separate;
		border-spacing: 0;
	}

	.dataTables_wrapper .dataTables_filter 
	{
		float: left;
		text-align: right;
		padding-left: 607px;
		padding-bottom: 10px;
	}
	.dataTables_wrapper .dataTables_length 
	{
		float: left;
		padding-left: 200px;
	}
</style>

<script type="text/javascript" charset="uts-8">
	$(document).ready(function ()
	{
		$("#BatchTable").dataTable(
		{
			"destroy":true
		});	
		
	}); 
</script>