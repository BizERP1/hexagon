<?php
	
	$dataTable=1;
	
	include('includes/SQL_CommonFunctions.inc');
	include('includes/session.inc');
	$Title = _('Assign Production Line');	
	include('includes/header.inc');
	

	//Check if there is an argument value in the url
	if(isset($_GET['ProductionOrder']))
	{		
		$argument = explode(" ",$_GET['ProductionOrder']); //Split the argument into production order number and no.of items
		$_SESSION['POForProductionLine'] = $argument[0]; //Assign production order number to a session variable
		$_SESSION['NoOfItems'] = $argument[1]; //Assign number of items to a session variable
		$_POST['PONumber'] = $_SESSION['POForProductionLine']; //Assign production order number to Production Order Number field			
		
		//Select delivery date and item of production order
		$sql0 = "SELECT deliverydate, item FROM productionorders WHERE productionorderno = '".$_SESSION['POForProductionLine']."'"; 
		$result0 = DB_query($sql0);		
		$myrow0 = DB_fetch_array($result0);		
		$_POST['PODeliveryDate'] = $myrow0[0];
		$_POST['POItem'] = $myrow0[1];
		
		//Select list of production lines
		$sql1 = "SELECT * FROM productionlines"; 
		$result1 = DB_query($sql1);							
		
		$_POST['POItemNumber'] = $_SESSION['NoOfItems']; //Assign no.of items to No.of Items field
		$_POST['PLStartDate'] = date('d/m/Y');
		$_SESSION['Status1'] = 1;
	}
	else
	{
		$_SESSION['Status1'] = 0;
	}
	
	//If production manager is adding a new production order insert it into database 
	if(isset($_POST['AddProductionLine'])) 
	{	
		//Validations before inserting data	
		if($_POST['PONumber'] == '')
		{
			prnMsg( _('Please select a production order'),'warn');				
		}		
		elseif($_POST['BANumber'] == '')
		{
			prnMsg( _('Please enter No.of Batch Analysis'),'warn');				
		}
		elseif($_POST['BANumber'] == '0')
		{
			prnMsg( _('No.of Batch Analysis must be greater than 0'),'warn');				
		}
		elseif($_POST['PLStartDate'] == '')
		{
			prnMsg( _('Please select start date'),'warn');				
		}			
		else
		{				
				$WOQuantity = $_POST['POItemNumber'];
				$PLStartDate = FormatDateforSQL($_POST['PLStartDate']); //Convert date into sql database format
							
				$sql9 = "SELECT productionlinecapacity FROM productionlines WHERE productionlineid = '". $_POST['ProductionLine'] ."'";
				$result9 = DB_query($sql9);
				$myrow9 = DB_fetch_array($result9);
							
				$quo = $WOQuantity/$myrow9[0];
				$rem = $WOQuantity%$myrow9[0];
				$quo = floor($quo);				
				$date = date('Y');
				
				$noofwo = $quo;
				if($rem > 0)
				{
					$noofwo++;
				}
				
				$quo1 = $noofwo/$_POST['BANumber'];
				$rem1 = $noofwo%$_POST['BANumber'];
				
				$remitems = $WOQuantity;
				
				$j = 1;
				while($j <= $_POST['BANumber'])
				{	
					$banumber = GetNextTransNo(52,$db);
					$batch1 = str_pad($banumber, 4, '0', STR_PAD_LEFT);
					
					if($j == '1')
					{
						$n = $quo1+$rem1;					
					}
					else
					{
						$n = $quo1;					
					}			
					
					if($j == '1')
					{
						$intpart = $myrow9[0];
						$noofitems = $intpart;
					}	
					
													
					$i = 1;	
					
											
					while($i <= $n)
					{	
						if($remitems<$noofitems)
						{
							$noofitems = $remitems;
						}				
						$wonumber = GetNextTransNo(40,$db);				
						$batch2 = str_pad($i, 4, '0', STR_PAD_LEFT);
						$batchanalysis = "HNEPL/". $batch1 ."/". $date;
						$batchnumber = "HNEPL/". $batch1 ."/". $batch2 ."/". $date;
						
						$sql6 = "INSERT INTO assignproductionline VALUES ('". $wonumber ."','". $_POST['PONumber'] ."','". $batchnumber ."','". $batchanalysis ."','". $_POST['POItem'] ."','". $noofitems ."','". $_POST['ProductionLine'] ."','". $PLStartDate ."','". $_POST['PODeliveryDate'] ."','0')";
						$result6 = DB_query($sql6);
													
						$sql4 = "INSERT INTO workorders (wo, loccode, requiredby, startdate) VALUES ('" . $wonumber . "', '" . $_POST['Location'] . "', '" . $_POST['PODeliveryDate'] . "', '" . $PLStartDate . "')";
						$result4 = DB_query($sql4);
						
						$CostResult = DB_query("SELECT SUM((materialcost+labourcost+overheadcost)*bom.quantity) AS cost
																FROM stockmaster INNER JOIN bom
																ON stockmaster.stockid=bom.component
																WHERE bom.parent='" . $_POST['POItem'] . "'
																AND bom.loccode='" . $_POST['Location'] . "'");
						$CostRow = DB_fetch_row($CostResult);
							
						if(is_null($CostRow[0]) OR $CostRow[0]==0) 
						{
							$Cost =0;
							prnMsg(_('In automatically creating a work order for') . ' ' . $_POST['POItem'] . ' ' . _('an item on this sales order, the cost of this item as accumulated from the sum of the component costs is nil. This could be because there is no bill of material set up ... you may wish to double check this'),'warn');
						} 
						else 
						{
							$Cost = $CostRow[0];
						}

						$sql5 = "INSERT INTO woitems (wo, stockid, qtyreqd, stdcost) VALUES ('" . $wonumber . "', '" . $_POST['POItem'] . "', '" . $noofitems . "', '0')";
						$result5 = DB_query($sql5);

						WoRealRequirements($db, $wonumber, $_POST['Location'], $_POST['POItem']);
						
						$sql16 = "SELECT controlled,serialised,woserialcount FROM stockmaster WHERE stockid='".$_POST['POItem']."'";
						$result16 = DB_query($sql16);
						$myrow16 = DB_fetch_array($result16);
						
						if($myrow16[1] == '1') 
						{				
							$count1 = $myrow16[2];
							$date1 = date('Y');
							for ($k=1;$k<=$noofitems;$k++) 
							{								
								$count2 = str_pad($count1, 4, '0', STR_PAD_LEFT);
								$serial = $_POST['POItem'].'/'.$count2.'/'.$date1;
								$sql10 = "INSERT INTO woserialnos (stockid, wo, qualitytext, quantity, serialno) VALUES ('" . $_POST['POItem'] . "', '" . $wonumber . "', '', '" . $noofitems .  "', '" . $serial . "')";			
								$result10 = DB_query($sql10);	
								$count1++;
							}	
							
							$sql17 = "UPDATE stockmaster SET woserialcount='".$count1."' WHERE stockid='".$_POST['POItem']."'";
							$result17 = DB_query($sql17);
						}
						elseif($myrow16[0] == '1')
						{
							$sql10 = "INSERT INTO woserialnos (stockid, wo, qualitytext, quantity, serialno) VALUES ('" . $_POST['POItem'] . "', '" . $wonumber . "', '', '" . $noofitems .  "', '" . $batchnumber . "')";			
							$result10 = DB_query($sql10);	
						}	
						$i++;

						if($remitems>$noofitems)
						{
							$remitems = $remitems - $noofitems;
						}															
					}
								
					$sql14 = "UPDATE productionorders SET status = '1' WHERE productionorderno = '". $_POST['PONumber'] ."'";
					$result14 = DB_query($sql14);						
					
					$j++;
				}	
				
				
						
		}	
		
		unset($_POST['BANumber']);
		unset($_POST['Locations']);
		unset($_POST['PLStartDate']);		
		unset($_SESSION['SelectedProductionLine']);
		
		//Select list of production lines
		$sql1 = "SELECT * FROM productionlines"; 
		$result1 = DB_query($sql1);	
		
		$_POST['PLStartDate'] = date('d/m/Y');
		
	}
	
	echo 	'<form action="', htmlspecialchars($_SERVER['PHP_SELF'], ENT_QUOTES, 'UTF-8'), '" method="post">','<input type="hidden" name="FormID" value="', $_SESSION['FormID'], '" />';
	
	echo 	'<p class="page_title_text">
				<img src="'.$RootPath.'/css/'.$Theme.'/images/customer.png" title="' . _('Production Line') . '" alt="" />' . ' ' . _('Production Line') . '
			</p>';
		  
	echo 	'<table>
				<tbody>
					<tr>
						<td valign="top">
							<table class="selection">
								<tr>
									<td colspan=5>
										<p class="page_title_text">' . _('Select Production Order') . '</p>
									</td>
								</tr>
								<tr>
								<td>' . _('Production Order No') . ':</td>
								<td>
									<input type="text" name="OrderNumber" value="' . $_POST['OrderNumber'] . '" size="12" maxlength="12"/>
								</td>
								<td>' . _('From Location') . ':</td>
								<td>
									<select name="OrderLocation">';
										//Select list of locations
										$sql2 = "SELECT loccode, locationname FROM locations"; 
										$result2 = DB_query($sql2);
										
										while($myrow2 = DB_fetch_array($result2))
										{		
											echo	'<option value="'. $myrow2['loccode'] .'">' . $myrow2['locationname'] . '</option>';																								
										}	
												
									echo '</select>
								</td>
								<td>
									<input type="submit" name="SelectProductionOrder" value="' . _('Select') . '" />
								</td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>';		
if($_SESSION['Status1'] == 1)
{	
	echo 	'<table>
				<tbody>
					<tr>
						<td valign="top">
							<table class="selection">								
								<tr>
									<td colspan=8>
										<p></p>
									</td>
								</tr>
								<tr>
									<td colspan=8>
										<p class="page_title_text">' . _('Production Order Details') . '</p>
									</td>
								</tr>
								<tr>
									<td style="text-align:right">' . _('Production Order Number') . ':</td>
									<td style="text-align:left"><input type="text" style="background-color:transparent;border:none;font-weight: bold" readonly="true" name="PONumber" value="' . $_POST['PONumber'] . '" size="12" maxlength="10"/></td>
									<td style="text-align:right">' . _('Delivery Date') . ':</td>
									<td style="text-align:left"><input type="text" style="background-color:transparent;border:none;font-weight: bold" readonly="true" name="PODeliveryDate" value="' . $_POST['PODeliveryDate'] . '" size="12" maxlength="10"/></td>								
									<td style="text-align:right">' . _('Item') . ':</td>
									<td style="text-align:left"><input type="text" style="background-color:transparent;border:none;font-weight: bold" readonly="true" name="POItem" value="' . $_POST['POItem'] . '" size="12" maxlength="10"/></td>	
									<td style="text-align:right">' . _('Qty') . ':</td>
									<td style="text-align:left"><input type="text" style="background-color:transparent;border:none;font-weight: bold" readonly="true" name="POItemNumber" value="' . $_POST['POItemNumber'] . '" size="12" maxlength="10"/></td>
								</tr>
								
								<tr>
									<td colspan=8>
										<p>
											<input type="submit" style="display:none" name="HiddenButton" value=""/>											
										</p>
									</td>
								</tr>
								<tr>
									<td colspan=10>
										<p class="page_title_text">' . _('Enter Production Line Details') . '</p>
									</td>
								</tr>
								<tr>							
									<td>' . _('No.of Batch Analysis') . ':</td>
									<td><input type="text" name="BANumber" value="' . $_POST['BANumber'] . '" size="12" maxlength="12"/></td>									
									<td>' . _('Start Date') . ':</td>
									<td><input class="date" alt="', $_SESSION['DefaultDateFormat'], '" name="PLStartDate" value="' . $_POST['PLStartDate'] . '" onchange="isDate(this, this.value, ', "'", $_SESSION['DefaultDateFormat'], "'", ')" size="12" maxlength="12"/></td>
									<td>' . _('Location') . ':</td>
									<td>
										<select name="Location">';
										
											//Select list of locations
											$sql2 = "SELECT loccode, locationname FROM locations"; 
											$result2 = DB_query($sql2);
											
											while($myrow2 = DB_fetch_array($result2))
											{		
												echo	'<option value="'. $myrow2['loccode'] .'">' . $myrow2['locationname'] . '</option>';																								
											}	
											
										echo '</select>
									</td>
									<td>' . _('Production Line') . ':</td>
									<td>
										<select name="ProductionLine">';
																									
											while($myrow1 = DB_fetch_array($result1))
											{														
												echo	'<option value="'. $myrow1['productionlineid'] .'">' . $myrow1['productionlinecapacity'] . '</option>';																								
											}																					
																												
										echo '</select>
									</td>									
								</tr>
								<tr>
									<td colspan=8>
										<p></p>
									</td>
								</tr>
								<tr>									
									<td colspan=8 style="text-align:center;">
										<input type="submit" name="AddProductionLine" value="' . _('Add Production Line') . '" />										
									</td>									
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>';	
}	
	if(isset($_POST['SelectProductionOrder']))
	{		
		if($_POST['OrderNumber'] != '')
		{
			$sql11 = "SELECT * FROM productionorders  WHERE status = '0' AND fromstkloc = '". $_POST['OrderLocation'] ."' AND productionorderno = '". $_POST['OrderNumber'] ."'";
			$result11 = DB_query($sql11);
			$numrows11 = DB_num_rows($result11);			
		}
		else
		{
			$sql11 = "SELECT * FROM productionorders WHERE status = '0' AND fromstkloc = '". $_POST['OrderLocation'] ."'"; 
			$result11 = DB_query($sql11);
			$numrows11 = DB_num_rows($result11);		
		}
				
		if($numrows11>0)
		{
			echo  	'<table cellpadding="2" class="selection" rules="all" id="POTable">
						<thead>			
							<tr>
								<th class="ascending">' . _('Sales Order No') . '</th>
								<th class="ascending">' . _('Production Order No') . '</th>
								<th class="ascending">' . _('Item') . '</th>
								<th class="ascending">' . _('Qty') . '</th>
								<th class="ascending">' . _('Mode of Transport') . '</th>
								<th class="ascending">' . _('Delivery Date') . '</th>								
								<th class="ascending"></th>	
							</tr>
						</thead>';
		}	
		else
		{
			prnMsg( _('No producion orders have been created'),'warn');
		}

		echo '<tbody>';
		
		while($myrow11 = DB_fetch_array($result11)) 
		{	
			$sql12 = "SELECT mbflag FROM stockmaster WHERE stockid = '". $myrow11['item'] ."'";
			$result12 = DB_query($sql12);
			$myrow12 = DB_fetch_array($result12);
		
			if($myrow12[0] == 'M')
			{
				$sql15 = "SELECT SUM(assignproductionline.noofitems*productionlines.productionlinecapacity) AS Total FROM assignproductionline INNER JOIN productionlines ON assignproductionline.productionlineid=productionlines.productionlineid WHERE assignproductionline.productionorderno='". $myrow3['productionorderno'] ."'";
				$result15 = DB_query($sql15);
				$myrow15 = DB_fetch_array($result15); //Calculate total no.of items in work order or production line
			
				$leftitems = $myrow11['noofitems']-$myrow15['Total']; //Calculate no of items left to be assigned to work order or production line
				$argument = $myrow11['productionorderno']." ".$leftitems; //Combining production order number and no.of items in each table row to be passed as a single argument
			
				
				$sql13 = "SELECT transportid, transportname FROM transportationmodes WHERE transportid = '".$myrow11['transport']."'";
				$result13 = DB_query($sql13);
				$myrow13 = DB_fetch_array($result13);
				
				printf('<tr>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>
						<td><button type="submit" name="PLAssign" value="'.$argument.'" >'. _('Assign Line Order') . '</button></td>
						</tr>',
						$myrow11['salesorderno'],
						$myrow11['productionorderno'],
						$myrow11['item'],
						$myrow11['noofitems'],
						$myrow13['transportname'],
						$myrow11['deliverydate']
				);				
			}		
		}
		
		echo '</tbody>';
		echo '</table>';	
		
		$_SESSION['Status1'] = 0;
	}
	
	if(isset($_POST['PLAssign']))
	{			
		echo '<meta http-equiv="Refresh" content="0; url=' . $RootPath . '/AssignProductionLine.php?ProductionOrder=' . $_POST['PLAssign'] . '">';
		$_SESSION['Status1'] = 1;
	}
	
	if(isset($_POST['AddProductionLine']))
	{	
		
		$sql7 = "SELECT * FROM assignproductionline WHERE productionorderno = '".$_SESSION['POForProductionLine']."'"; //Selecting previous production lines assigned for this production order if any
		$result7 = DB_query($sql7);
		$numrows7 = DB_num_rows($result7);		
		
		if($numrows7>0)
		{
			echo  	'<table cellpadding="2" class="selection" rules="all" id="PLTable">
						<thead>			
							<tr>
								<th class="ascending">' . _('Work Order No.') . '</th>
								<th class="ascending">' . _('Production Order No.') . '</th>
								<th class="ascending">' . _('Batch Analysis No.') . '</th>	
								<th class="ascending">' . _('Batch No.') . '</th>									
								<th class="ascending">' . _('Item') . '</th>
								<th class="ascending">' . _('Qty') . '</th>
								<th class="ascending">' . _('Production Line') . '</th>
								<th class="ascending">' . _('Start Date') . '</th>
								<th class="ascending">' . _('Delivery Date') . '</th>								
							</tr>
						</thead>';
		}				
	
		echo '<tbody>';
		
		//Loop through production orders
		while (($myrow7 = DB_fetch_array($result7))) 
		{
			$sql8 = "SELECT status FROM assignproductionline WHERE productionorderno = '". $myrow7['productionorderno'] ."'";
			$result8 = DB_query($sql8);
			$myrow8 = DB_fetch_array($result8);
			
			if($myrow8[0] == '0')
			{
				$sql9 = "SELECT productionlinecapacity FROM productionlines WHERE productionlineid = '". $myrow7['productionlineid'] ."'";
				$result9 = DB_query($sql9);
				$myrow9 = DB_fetch_array($result9);				 
			
				printf("<tr>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>	
						<td>%s</td>							
						<td>%s</td>					
						<td>%s</td>	
						<td>%s</td>	
						<td>%s</td>					
						</tr>",
						$myrow7['workorderno'],
						$myrow7['productionorderno'],
						$myrow7['batchanalysisno'],
						$myrow7['batchnum'],						
						$myrow7['item'],
						$myrow7['noofitems'],				
						$myrow9['productionlinecapacity'],
						$myrow7['startdate'],
						$myrow7['deliverydate']
				);		
			}
		}		
				
		echo '</tbody>';
		echo '</table>';	
	}		
	
	echo '</form>';
	
	include('includes/footer.inc');	
	
?>

<script src="DataTables/media/js/jquery.js" type="text/javascript" ></script>
<script src="DataTables/media/js/jquery.dataTables.js" type="text/javascript"> </script>

<style type="text/css">
	@import "DataTables/media/css/jquery.dataTables.css";
	table.dataTable 
	{
		width: 50%;
		margin: 0 auto;
		clear: both;
		border-collapse: separate;
		border-spacing: 0;
	}

	.dataTables_wrapper .dataTables_filter 
	{
		float: left;
		text-align: right;
		padding-left: 607px;
		padding-bottom: 10px;
	}
	.dataTables_wrapper .dataTables_length 
	{
		float: left;
		padding-left: 200px;
	}
</style>

<script type="text/javascript" charset="uts-8">
	$(document).ready(function ()
	{
		$("#PLTable").dataTable(
		{
			"destroy":true
		});
		
		$("#POTable").dataTable(
		{
			"destroy":true
		});
	}); 
</script>