<?php

	/* $Id: TransportationModes.php 6998 2014-11-22 02:28:56Z daintree $*/

	include('includes/session.inc');
	$Title = _('Transportation Modes');
	include('includes/header.inc');

	if (isset($_POST['SelectedType']))
	{
		$SelectedType = mb_strtoupper($_POST['SelectedType']);
	} 
	elseif (isset($_GET['SelectedType']))
	{
		$SelectedType = mb_strtoupper($_GET['SelectedType']);
	}

	if (isset($Errors)) 
	{
		unset($Errors);
	}

	$Errors = array();

	echo '<p class="page_title_text"><img src="'.$RootPath.'/css/'.$Theme.'/images/maintenance.png" title="' . _('Search') . '" alt="" />' . ' ' . $Title . '</p>';

	if (isset($_POST['submit'])) 
	{
		//initialise no input errors assumed initially before we test
		$InputError = 0;

		/* actions to take once the user has clicked the submit button ie the page has called itself with some user input */

		//first off validate inputs sensible
		$i=1;

		if (mb_strlen($_POST['TransportId']) > 2) 
		{
			$InputError = 1;
			prnMsg(_('The Transportation Id must be two characters or less long'),'error');
			$Errors[$i] = 'TransportationMode';
			$i++;
		}
		elseif ($_POST['TransportId']=='' OR $_POST['TransportId']==' ' OR $_POST['TransportId']=='  ') 
		{
			$InputError = 1;
			prnMsg( _('The transportation mode cannot be an empty string or spaces'),'error');
			$Errors[$i] = 'TransportationMode';
			$i++;
		} 
		elseif( trim($_POST['TransportName'])=='')
		{
			$InputError = 1;
			prnMsg (_('The transportation mode description cannot be empty'),'error');
			$Errors[$i] = 'TransportationMode';
			$i++;
		} 
		elseif (mb_strlen($_POST['TransportName']) >50) 
		{
			$InputError = 1;
			echo prnMsg(_('The transportation mode description must be fifty characters or less long'),'error');
			$Errors[$i] = 'TransportationMode';
			$i++;
		} 
		
		if (isset($SelectedType) AND $InputError !=1) 
		{
			$sql = "UPDATE transportationmodes SET transportname = '" . $_POST['TransportName'] . "' WHERE transportid = '".$SelectedType."'";
			$msg = _('The transportation mode') . ' ' . $SelectedType . ' ' .  _('has been updated');
		} 
		elseif ( $InputError !=1 ) 
		{
			// First check the type is not being duplicated

			$checkSql = "SELECT count(*) FROM transportationmodes WHERE transportid = '" . $_POST['TransportId'] . "'";
			$CheckResult = DB_query($checkSql);
			$CheckRow = DB_fetch_row($CheckResult);

			if ( $CheckRow[0] > 0 ) 
			{
				$InputError = 1;
				prnMsg( _('The transportation mode') . $_POST['TransportId'] . _(' already exist.'),'error');
			} 
			else 
			{
				// Add new record on submit
				
				$sql = "INSERT INTO transportationmodes (transportid, transportname) VALUES ('" . str_replace(' ', '', $_POST['TransportId']) . "','" . $_POST['TransportName'] . "')";
				$msg = _('Transportation mode') . ' ' . $_POST['TransportName'] .  ' ' . _('has been created');
				$checkSql = "SELECT count(transportid) FROM transportationmodes";
				$result = DB_query($checkSql);
				$row = DB_fetch_row($result);
			}
		}

		if ( $InputError !=1) 
		{
			//run the SQL from either of the above possibilites
			$result = DB_query($sql);

			// Check the default transportation mode exists
			$checkSql = "SELECT count(*) FROM transportationmodes WHERE transportid = '" . $_SESSION['TransportationMode'] . "'";
			$CheckResult = DB_query($checkSql);
			$CheckRow = DB_fetch_row($CheckResult);

			// If it doesnt then update config with newly created one.
			if ($CheckRow[0] == 0) 
			{
				$sql = "UPDATE config SET confvalue='".$_POST['TransportId']."' WHERE confname='TransportationMode'";
				$result = DB_query($sql);
				$_SESSION['TransportationMode'] = $_POST['TransportId'];
			}

			prnMsg($msg,'success');

			unset($SelectedType);
			unset($_POST['TransportId']);
			unset($_POST['TransportName']);
		}
	} 
	elseif ( isset($_GET['delete']) ) 
	{
		// PREVENT DELETES IF DEPENDENT RECORDS IN 'DebtorTrans'
		// Prevent delete if saletype exist in customer transactions

		$sql= "SELECT COUNT(*) FROM productionorders WHERE transport = '".$SelectedType."'";		
		$result = DB_query($sql);
		$myrow = DB_fetch_row($result);
		
		if ($myrow[0]>0) 
		{
			prnMsg(_('Cannot delete this transportation mode because production orders have been created using this transportation mode') . '<br />' . _('There are') . ' ' . $myrow[0] . ' ' . _('production orders using this transportation mode code'),'error');

		} 
		else 
		{			
			$sql="DELETE FROM transportationmodes WHERE transportid ='" . $SelectedType . "'";
			$ErrMsg = _('The transportation mode could not be deleted because');
			$result = DB_query($sql,$ErrMsg);
			prnMsg(_('Transportation mode') . ' ' . $SelectedType  . ' ' . _('has been deleted') ,'success');
				
			unset ($SelectedType);
			unset($_GET['delete']);
			
		} 
	}


	if(isset($_POST['Cancel']))
	{
		unset($SelectedType);
		unset($_POST['TransportId']);
		unset($_POST['TransportName']);
	}

	if (!isset($SelectedType))
	{
		/* It could still be the second time the page has been run and a record has been selected for modification - SelectedType will exist because it was sent with the new call. If its the first time the page has been displayed with no parameters
		then none of the above are true and the list of transportation modes will be displayed with
		links to delete or edit each. These will call the same page again and allow update/input
		or deletion of the records*/

		$sql = "SELECT transportid,transportname FROM transportationmodes ORDER BY transportid";
		$result = DB_query($sql);

		echo '<table class="selection">
			<tr>
					<th class="ascending">' . _('Type Code') . '</th>
					<th class="ascending">' . _('Type Name') . '</th>
			</tr>';

		$k=0; //row colour counter

		while ($myrow = DB_fetch_row($result)) 
		{
			if ($k==1)
			{
				echo '<tr class="EvenTableRows">';
				$k=0;
			} 
			else 
			{
				echo '<tr class="OddTableRows">';
				$k=1;
			}

			printf('<td>%s</td>
			<td>%s</td>
			<td><a href="%sSelectedType=%s">' . _('Edit') . '</a></td>
			<td><a href="%sSelectedType=%s&amp;delete=yes" onclick="return confirm(\'' . _('Are you sure you wish to delete this transportation mode ?') . '\');">' . _('Delete') . '</a></td>
			</tr>',
			$myrow[0],
			$myrow[1],
			htmlspecialchars($_SERVER['PHP_SELF'],ENT_QUOTES,'UTF-8') . '?', $myrow[0],
			htmlspecialchars($_SERVER['PHP_SELF'],ENT_QUOTES,'UTF-8') . '?', $myrow[0]);
		}
		//END WHILE LIST LOOP
		echo '</table>';
	}

	//end of ifs and buts!
	if (isset($SelectedType)) 
	{
		echo '<br />
				<div class="centre">
					<a href="' . htmlspecialchars($_SERVER['PHP_SELF'],ENT_QUOTES,'UTF-8') .'">' . _('Show All Transportation Modes Defined') . '</a>
				</div>';
	}
	if (! isset($_GET['delete']))
	{
		echo '<form method="post" action="' . htmlspecialchars($_SERVER['PHP_SELF'],ENT_QUOTES,'UTF-8') . '" >
				<div>
					<input type="hidden" name="FormID" value="' . $_SESSION['FormID'] . '" />
				<br />';


		// The user wish to EDIT an existing type
		if ( isset($SelectedType) AND $SelectedType!='' ) 
		{
			$sql = "SELECT transportid, transportname FROM transportationmodes WHERE transportid='" . $SelectedType . "'";
			$result = DB_query($sql);
			$myrow = DB_fetch_array($result);

			$_POST['TransportId'] = $myrow['TransportId'];
			$_POST['TransportName']  = $myrow['TransportName'];

			echo '<input type="hidden" name="SelectedType" value="' . $SelectedType . '" />
				  <input type="hidden" name="TransportId" value="' . $_POST['TransportId'] . '" />
						<table class="selection">
						<tr>
						<th colspan="4"><b>' . _('Transportation Mode') . '</b></th>
					</tr>
					<tr>
						<td>' . _('Type Code') . ':</td>
						<td>' . $_POST['TransportId'] . '</td>
					</tr>';

		} 
		else 	
		{
			// This is a new type so the user may volunteer a type code

			echo '<table class="selection">
					<tr>
						<th colspan="4"><b>' . _('Transportation Mode') . '</b></th>
					</tr>
					<tr>
						<td>' . _('Transportation Id') . ':</td>
						<td><input type="text" ' . (in_array('TransportationMode',$Errors) ? 'class="inputerror"' : '' ) .' size="3" maxlength="2" name="TransportId" /></td>
					</tr>';
		}

		if (!isset($_POST['TransportName'])) 
		{
			$_POST['TransportName']='';
		}
		
		echo '<tr>
				<td>' . _('Transportation Mode Name') . ':</td>
				<td><input type="text" name="TransportName" value="' . $_POST['TransportName'] . '" /></td>
			</tr>
			</table>'; // close main table

		echo '<br /><div class="centre"><input type="submit" name="submit" value="' . _('Accept') . '" /><input type="submit" name="Cancel" value="' . _('Cancel') . '" /></div>
				</div>
			  </form>';

	} // end if user wish to delete

	include('includes/footer.inc');
?>